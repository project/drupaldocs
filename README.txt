
THIS MODULE IS STILL JUST A PROOF ON CONCEPT AND NOT NEAR COMPLETE.

This documentation module provides basic terminology and instruction for doing
common Drupal site maintenance tasks. It is not currently synchronized with
the Drupal.org handbooks, so you should always refer back to Drupal.org
(http://drupal.org/handbooks) for the latest information.

REQUIREMENTS
------------
This module depends upon the Advanced Help module
(http://drupal.org/project/advanced_help). Make sure you install it before
attempting to use this module.

INSTALLATION AND USE
--------------------
1. Download and install the module like any other Drupal module.
2. Enable the module at Administer > Site building > Modules 
   (admin/build/modules)
3. Make sure the appropriate roles have the correct Advanced help permissions
   at Administer > User management > Permissions (admin/user/permissions), in
   the "advanced_help module" section.
3. Go to Administer > Advanced help > Drupal documentation   
   (admin/advanced_help/documentation) to read.

To learn more about how Advanced help works and the format for adding documentation to this module, read Administer > Advanced help > Advanced help (admin/advanced_help/advanced_help)